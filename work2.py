import sys

args = sys.argv

def if_test(name, sex='不明'):
    if sex == "女":
        print('* [女] *')
        print('**********')
        print('* [' + name + '] *')
        print('**********')
    elif sex == "男":
        print('- [男] -')
        print('----------')
        print('- [' + name + '] -')
        print('----------')
    else:
        print('%%%%%%%%%')
        print('% [' + name + '] %')
        print('%%%%%%%%%')

if_test(args[1], args[2])


# if __name__ == '__main__':
#     if 2 == len(args):
#         foo(args[1], args[2])
#     elif 1 == len(args):
#         foo(args[1])
#     else:
#         print('Arguments are too short')

#     if 2 <= len(args):
#     if args[1].isdigit():
#         foo(args[1], args[2])
#     else:
#         print('Argument is not digit')
# else:
#     print('Arguments are too short')



